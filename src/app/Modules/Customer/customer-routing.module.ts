import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CustomerTableComponent } from './Components/customer-table/customer-table/customer-table.component';
 
const routes: Routes = [
  {
    path: '',
    component: CustomerTableComponent,
  }
];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { constructor() {
  console.log('CustomerRoutingModule loaded.');
}}