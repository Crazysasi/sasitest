import { Injectable } from '@angular/core';
import { ProductDataService } from '../data/product-data.service';

@Injectable({
  providedIn: 'root'
})
export class ProductViewService {

  constructor(private productDataService:ProductDataService) { }

  getProductData(){
    return this.productDataService.getProductData();
  }

}
