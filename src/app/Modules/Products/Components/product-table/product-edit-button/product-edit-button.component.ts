import { Component, OnInit, ViewChild, AfterViewChecked, TemplateRef } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductUpdateDetailsComponent } from '../product-update-details/product-update-details.component';
import { forwardRef } from '@angular/core';
import { ProductUserDetailsComponent } from '../product-user-details/product-user-details.component';
import { Observable } from "rxjs";
@Component({
  selector: 'app-product-edit-button',
  templateUrl: './product-edit-button.component.html',
  styleUrls: ['./product-edit-button.component.css']
})
export class ProductEditButtonComponent implements ICellRendererAngularComp {
  //@ViewChild(forwardRef(() => ProductTableComponent)) singleRowData: ProductTableComponent;
  params;
  label: string;
  isavailable = true;
  test = "s"
  gridApi: any
  gridColumnApi: any
  dialogData: any
  agInit(params): void {
    this.params = params;
    console.log("params", this.params)
    this.label = this.params.label || null;
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.dialogData = this.dialogData;
  }
 
  constructor(public dialog: MatDialog) { }
 
  openDialog(): void {
    console.log("params", this.params.data)
    const dialogRef = this.dialog.open(ProductUpdateDetailsComponent, {
      width: '700px',
      height: 'auto',
      data: {
        product_id: this.params.data.product_id,
        product_name: this.params.data.product_name,
        product_price: this.params.data.product_price,
        product_quantity: this.params.data.product_quantity,
        product_pincode: this.params.data.product_pincode,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      // this.dialogData = result;
      // this.updateRow(result);
    });
 
    this.isavailable = true;
  }
  deleteRow(result) {
    var selectedData = this.gridApi.getSelectedRows();
    var res = this.gridApi.updateRowData({ remove: selectedData });
  }
  refresh(params?: any): boolean {
    return true;
  }
  // onClick($event) {
  //   console.log($event);
 
  //   if (this.params.onClick instanceof Function) {
  //     // put anything into params u want pass into parents component
  //     const params = {
  //       event: $event,
  //       rowData: this.params.node.data
  //       // ...something
  //     }
  //     console.log('params',this.params.data)
  //     this.params.onClick(params);
 
  //   }
  // }
  // ngAfterViewInit() {
  //   console.log('rowdatac', this.singleRowData)
  // }
  // ngAfterViewChecked()           {
  //   console.log('rowdatacc', this.singleRowData)
  // }
 
 
}
